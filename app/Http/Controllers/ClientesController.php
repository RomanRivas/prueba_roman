<?php

namespace App\Http\Controllers;

use App\Cliente;
use Illuminate\Http\Request;

class ClientesController extends Controller
{

    public function __construct()
        {
            $this->middleware('auth');
        }

	public function create(Request $request)
    	{
    		$this->validate($request, [
    			'nombre' => 'required',
    			'documento' => 'required',
    			'correo' => 'required',
    			'direccion' => 'required',
    		], [
    			'nombre.required' => 'Campo nombre obligatorio',
    			'documento.required' => 'Campo documento obligatorio',
    			'correo.required' => 'Campo correo obligatorio',
    			'direccion.required' => 'Campo direccion obligatorio',
    		]);

    		$cliente = Cliente::create([
    			'nombre' => $request->input('nombre'),
    			'documento' => $request->input('documento'),
    			'correo' => $request->input('correo'),
    			'direccion' => $request->input('direccion')
    		]);

    		return redirect('/cliente/list');

    	}

    public function update(Request $request, $id)
        {
            $this->validate($request, [
                'nombre' => 'required',
                'documento' => 'required',
                'correo' => 'required',
                'direccion' => 'required',
            ], [
                'nombre.required' => 'Campo nombre obligatorio',
                'documento.required' => 'Campo documento obligatorio',
                'correo.required' => 'Campo correo obligatorio',
                'direccion.required' => 'Campo direccion obligatorio',
            ]);

            $cliente = Cliente::findOrFail($id);

            $cliente->update([
                'nombre' => $request->input('nombre'),
                'documento' => $request->input('documento'),
                'correo' => $request->input('correo'),
                'direccion' => $request->input('direccion')
            ]);

            return redirect('/cliente/list');

        }

    public function list()
    	{
    		$clientes = Cliente::all();

    		return view('clientes.list', [
    			'clientes' => $clientes,
    		]);
    	}

    public function delete($id)
        {
            $cliente = Cliente::findOrFail($id);
            $cliente->delete();
            return redirect('/cliente/list')->with('success', 'Se eliminó exitosamente');

        }

    public function edit($id)
        {
            $cliente = Cliente::findOrFail($id);
            return view('clientes.edit', [
                'cliente' => $cliente,
            ]);

        }
}
