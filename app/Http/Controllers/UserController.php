<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function __construct()
        {
            $this->middleware('auth');
        }
        
    public function create(Request $request)
        {
            $this->validate($request, [
                'name' => 'required',
                'email' => 'required',
                'rol' => '',
                'password' => 'required',
            ], [
                'name.required' => 'Campo nombre obligatorio',
                'email.required' => 'Campo email obligatorio',
                'password.required' => 'Campo direccion obligatorio',
            ]);

            $user = User::create([
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'rol' => $request->input('rol'),
                'password' => Hash::make($request->input('password'))
            ]);

            return redirect('/user/list');

        }

    public function update(Request $request, $id)
        {
            $this->validate($request, [
                'name' => 'required',
                'email' => 'required',
                'rol' => '',
            ], [
                'name.required' => 'Campo nombre obligatorio',
                'email.required' => 'Campo email obligatorio',
            ]);

            $user = User::findOrFail($id);

            $user->update([
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'rol' => $request->input('rol')
            ]);

            return redirect('/user/list');

        }

    public function list()
        {
            $users = User::all();

            return view('users.list', [
                'users' => $users,
            ]);
        }

    public function delete($id)
        {
            $user = User::findOrFail($id);
            $user->delete();

            return redirect('/user/list')->with('success', 'Se eliminó exitosamente');

        }

    public function edit($id)
        {
            $user = User::findOrFail($id);
            return view('users.edit', [
                'user' => $user,
            ]);

        }
}
