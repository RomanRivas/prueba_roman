@extends('admin.layout')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <h3 style="color: #922BEA">Editar Cliente</h3>	
                <div class="box" style="padding: 50px ">
                    <form method="POST" action="{{route('cliente.update', $cliente->id)}}">
                        @csrf

                        <div class="form-group row">
                            <label for="nombre" class="col-md-4 col-form-label text-md-right">Nombre</label>

                            <div class="col-md-6">
                            	<div class="form-group">
				                    <input type="text" name="nombre" class="form-control @if($errors->has('nombre')) is-invalid @endif" value="{{ $cliente->nombre }}" required>
				                    @if($errors->has('nombre'))
				                        @foreach($errors->get('nombre') as $error)
				                            <div class="invalid-feedback" role="alert">
				                                {{$error}}
				                            </div>
				                        @endforeach
				                    @endif
				                </div>
                                
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="documento" class="col-md-4 col-form-label text-md-right">Documento</label>

                            <div class="col-md-6">
                            	<div class="form-group">
				                    <input type="text" name="documento" class="form-control @if($errors->has('documento')) is-invalid @endif" value="{{ $cliente->documento }}" required>
				                    @if($errors->has('documento'))
				                        @foreach($errors->get('documento') as $error)
				                            <div class="invalid-feedback" role="alert">
				                                {{$error}}
				                            </div>
				                        @endforeach
				                    @endif
				                </div>
                                
                            </div>
                        </div>
						
						<div class="form-group row">
                            <label for="correo" class="col-md-4 col-form-label text-md-right">Correo</label>

                            <div class="col-md-6">
                            	<div class="form-group">
				                    <input type="email" name="correo" class="form-control @if($errors->has('correo')) is-invalid @endif" value="{{ $cliente->correo }}" required>
				                    @if($errors->has('correo'))
				                        @foreach($errors->get('correo') as $error)
				                            <div class="invalid-feedback" role="alert">
				                                {{$error}}
				                            </div>
				                        @endforeach
				                    @endif
				                </div>
                                
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="direccion" class="col-md-4 col-form-label text-md-right">Direccion</label>

                            <div class="col-md-6">
                            	<div class="form-group">
				                    <input type="text" name="direccion" class="form-control @if($errors->has('direccion')) is-invalid @endif" value="{{ $cliente->direccion }}" required>
				                    @if($errors->has('direccion'))
				                        @foreach($errors->get('direccion') as $error)
				                            <div class="invalid-feedback" role="alert">
				                                {{$error}}
				                            </div>
				                        @endforeach
				                    @endif
				                </div>
                                
                            </div>
                        </div>
                        

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">Guardar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection