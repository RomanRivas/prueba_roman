@extends('admin.layout')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-11">
            <div class="card">
                <h3 style="color: #922BEA">Listado Clientes</h3>   
                <div class="box" style="padding: 50px ">
                @if (session('success')) <div class="alert alert-success"> {{ session('success') }} </div> @endif
                <table id="MyTable" class="table table-hover table-striped">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Documento</th>
                            <th>Correo</th>
                            <th>Direccion</th>
                            <th>Editar</th>
                            <th>Eliminar</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($clientes as $cliente)
                            <tr>
                                <td>{{ $cliente->nombre}}</td>
                                <td>{{ $cliente->documento}}</td>
                                <td>{{ $cliente->correo}}</td>
                                <td>{{ $cliente->direccion}}</td>
                                <td><a href="{{route('cliente.edit', $cliente->id)}}" class="btn btn-warning"><i class="fa fa-edit"></i></a></td>
                                <td>
                                    <form action="{{route('cliente.delete', $cliente->id)}}" method="post">
                                        @csrf
                                        <button class="btn btn-danger"><i class="fa fa-trash"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                
        </div>
    </div>
</div>
</div>
</div>
@endsection