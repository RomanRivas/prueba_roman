@extends('admin.layout')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <h3 style="color: #922BEA">Crear Usuario</h3>   
                <div class="box" style="padding: 50px ">
                    <form method="POST" action="{{route('user.create')}}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Nombre</label>

                            <div class="col-md-6">
                            	<div class="form-group">
				                    <input type="text" name="name" class="form-control @if($errors->has('name')) is-invalid @endif" placeholder="Juan Felipe Garcia" value="{{ old('name') }}" required>
				                    @if($errors->has('name'))
				                        @foreach($errors->get('name') as $error)
				                            <div class="invalid-feedback" role="alert">
				                                {{$error}}
				                            </div>
				                        @endforeach
				                    @endif
				                </div>
                                
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">Email</label>

                            <div class="col-md-6">
                            	<div class="form-group">
				                    <input type="text" name="email" class="form-control @if($errors->has('email')) is-invalid @endif" placeholder="1128567484" value="{{ old('email') }}" required>
				                    @if($errors->has('email'))
				                        @foreach($errors->get('email') as $error)
				                            <div class="invalid-feedback" role="alert">
				                                {{$error}}
				                            </div>
				                        @endforeach
				                    @endif
				                </div>
                                
                            </div>
                        </div>
						
						<div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">Password</label>

                            <div class="col-md-6">
                            	<div class="form-group">
				                    <input type="password" name="password" class="form-control @if($errors->has('password')) is-invalid @endif" value="{{ old('password') }}" required>
				                    @if($errors->has('password'))
				                        @foreach($errors->get('password') as $error)
				                            <div class="invalid-feedback" role="alert">
				                                {{$error}}
				                            </div>
				                        @endforeach
				                    @endif
				                </div>
                                
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="rol" class="col-md-4 col-form-label text-md-right">Rol</label>

                            <div class="col-md-6">
                            	<div class="form-group">
				                    <select name="rol" class='form-control'>
                                        <option value="1">Admin</option>
                                        <option value="3">Vendedor</option>
                                    </select>
				                </div>
                                
                            </div>
                        </div>
                        

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">Guardar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection