@extends('admin.layout')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-11">
            <div class="card">
                <h3 style="color: #922BEA">Listado Usuarios</h3>   
                <div class="box" style="padding: 50px ">	
                @if (session('success')) <div class="alert alert-success"> {{ session('success') }} </div> @endif
                <table id="MyTable" class="table table-hover table-striped">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Email</th>
                            <th>Rol</th>
                            <th>Editar</th>
                            <th>Eliminar</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>{{ $user->name}}</td>
                                <td>{{ $user->email}}</td>
                                <td>
                                    @if($user->rol == 1)
                                        Admin
                                    @else
                                        Vendedor
                                    @endif
                                </td>
                                <td><a href="{{route('user.edit', $user->id)}}" class="btn btn-warning"><i class="fa fa-edit"></i></a></td>
                                <td>
                                    <form action="{{route('user.delete', $user->id)}}" method="post">
                                        @csrf
                                        <button class="btn btn-danger"><i class="fa fa-trash"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                
        </div>
    </div>
</div>
@endsection