<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { return view('home'); });



//clientes
Route::get('/cliente/create', function () { return view('clientes.create'); })->name('cliente.create');

Route::get('/cliente/edit/{id}', 'ClientesController@edit')->name('cliente.edit');

Route::post('/cliente/create', 'ClientesController@create');
Route::post('/cliente/delete/{id}', 'ClientesController@delete')->name('cliente.delete');
Route::post('/cliente/update/{id}', 'ClientesController@update')->name('cliente.update');


Route::group([
    'middleware' => 'admin',
    'prefix' => 'user'
], function () {
    //usuarios
	Route::get('/create', function () { return view('users.create'); })->name('user.create');
	Route::get('/list', 'UserController@list')->name('user.list');
	Route::get('/edit/{id}', 'UserController@edit')->name('user.edit');

	Route::post('/create', 'UserController@create');
	Route::post('/delete/{id}', 'UserController@delete')->name('user.delete');
	Route::post('/update/{id}', 'UserController@update')->name('user.update');
});





//fin de clientes

//Auth
Auth::routes();
Route::get('/homepanel', function () {return view('adminpanel');})->name('adminpanel');
Route::get('/cliente/list', 'ClientesController@list')->name('cliente.list');

Route::get('/home', 'HomeController@index')->name('home');
